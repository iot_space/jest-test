export const calculator = (param) => {
  if (param.length === 0) {
    return 0
  }
  const numbers = param.match(/-?\d+/g).map(Number)
  let sum = 0
  const invalidNumbers = []
  numbers.forEach((number) => {
    if (number <= 1000) {
    if (number < 0) {
      invalidNumbers.push(number)
    }
    sum = sum + number
  }
  })
  if (invalidNumbers.length > 0) {
    return invalidNumbers
  }
  if (sum > 1000) {
    return 0
  }
  return sum
}
