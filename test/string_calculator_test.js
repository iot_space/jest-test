import {calculator} from '../src/string_calculator';

test('An empty string produces 0', () => {
    expect(calculator("")).toBe(0);
});
test('An int string with one number returns number', () => {
  expect(calculator("1asdsad")).toBe(1);
});
test('Strings numbers separated with , returns numbers sum', () => {
  expect(calculator("1,2")).toBe(3);
});
test('Strings numbers separated with \n returns numbers sum', () => {
  expect(calculator("1\n2")).toBe(3);
});
test('custom delimiter can be defined by prefixing the input string', () => {
  expect(calculator("//;\n1:2")).toBe(3);
});

test('custom delimiter can be defined by prefixing the input string', () => {
  expect(calculator("//;\n-11:-2")).toEqual([-11,-2]);
});
test('Ignore numbers greater than 1000', () => {
  expect(calculator("//;\n1100aa1")).toEqual(1);
  expect(calculator("//;\n1100aa")).toEqual(0);
  expect(calculator("//;\n501aa500")).toEqual(0);
});

test('custom delimiter of any length', () => {
  expect(calculator("//[|||]\n1|||2|||3")).toEqual(6);
});

test('multiple delimiters can be defined by prefixing the input string', () => {
  expect(calculator("//[|][;;]\n1|2;;3")).toEqual(6);
});